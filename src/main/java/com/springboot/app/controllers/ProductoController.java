package com.springboot.app.controllers;




import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.app.models.Producto;

import com.springboot.app.services.IProductoService;

@RestController
@RequestMapping("/api")
public class ProductoController {
	
	
	@Autowired 
	private IProductoService productoService;
	
	//obtener todos los productos
	@GetMapping("/productos")
	public List<Producto> getAllProductos(){
		
		return productoService.getAllProductos();
	}
	
	//obtener productos por id
	@GetMapping("/productos/{id}")
	public Optional<Producto> getProductoById(@PathVariable Long id){
		
		return productoService.getProductoById(id);
	}
	
	//guardar productos
	@PostMapping("/productos")
	public Producto guardarProducto(@RequestBody Producto prod) {
		
		return productoService.guardarProducto(prod);
	}
	
	//actualizar usuarios
	@PutMapping("/productos")
	public Producto actualizarProducto(@RequestBody Producto prod) {
		
		Producto productoActualizado = new Producto();
		//Si no existe el id no sigue
		if(prod.getId()==null) {
			return null;
		}
		
		//existe el id del usuario 
		//validar que el id exista en la bd
		
		Optional<Producto> usuBd = productoService.getProductoById(prod.getId());
		
		if ( usuBd.isPresent()) {
			productoActualizado = productoService.guardarProducto(prod);
		}
		
		return productoActualizado;
		
	}
	
	//Eliminar usuario
		@DeleteMapping("/productos")
		public Boolean eliminarProducto(@RequestBody Producto prod) {
			
			return productoService.eliminarProducto(prod);
		}
		
	
	
	
	
}
