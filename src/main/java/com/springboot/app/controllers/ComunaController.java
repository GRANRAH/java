package com.springboot.app.controllers;




import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.app.models.Comuna;

import com.springboot.app.services.IComunaService;


@RestController
@RequestMapping("/api")
public class ComunaController {
	
	
	@Autowired 
	private IComunaService comunaService;
	
	//obtener todos las comunas
	@GetMapping("/comunas")
	public List<Comuna> getAllComunas(){
		
		return comunaService.getAllComunas();
	}
	
	//obtener comunas por id
	@GetMapping("/comunas/{id}")
	public Optional<Comuna> getComunaById(@PathVariable Long id){
		
		return comunaService.getComunaById(id);
	}
	
	//guardar comunas
	@PostMapping("/comunas")
	public Comuna guardarComuna(@RequestBody Comuna com) {
		
		return comunaService.guardarComuna(com);
	}
	
	//actualizar comunas
	@PutMapping("/comunas")
	public Comuna actualizarProducto(@RequestBody Comuna com) {
		
		Comuna comunaActualizado = new Comuna();
		//Si no existe el id no sigue
		if(com.getId()==null) {
			return null;
		}
		
		//existe el id de comuna 
		//validar que el id exista en la bd
		
		Optional<Comuna> usuBd = comunaService.getComunaById(com.getId());
		
		if ( usuBd.isPresent()) {
			comunaActualizado = comunaService.guardarComuna(com);
		}
		
		return comunaActualizado;
		
	}
	
	//Eliminar comuna
		@DeleteMapping("/comunas")
		public Boolean eliminarComuna(@RequestBody Comuna com) {
			
			return comunaService.eliminarComuna(com);
		}
		
	
	
	
	
}
