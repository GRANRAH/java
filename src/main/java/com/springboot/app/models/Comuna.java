package com.springboot.app.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name ="comunas")
public class Comuna implements Serializable{

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	
	
		private Long id;
		private String nombre;
		@OneToMany(
				fetch = FetchType.LAZY,
				cascade = CascadeType.ALL,
				mappedBy = "comuna"
				
				)
		@JsonIgnoreProperties({"comuna"})//arreglo de usuarios no me traigas comuna
		private List<Usuario> usuarios;  //por que la comuna al traer usuarios este viene con comuna originalmente 
										//y se produce un bucle por que usuarios trae nuevamente comuna y asi sucesivamente
public List<Usuario> getUsuarios() {
			return usuarios;
		}

		public void setUsuarios(List<Usuario> usuarios) {
			this.usuarios = usuarios;
		}

public Comuna() {
	
		this.usuarios = new ArrayList<Usuario>();
	
	}

 

	public Comuna(Long id, String nombre, List<Usuario> usuarios) {
	super();
	this.id = id;
	this.nombre = nombre;
	this.usuarios = usuarios;
}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	private static final long serialVersionUID = 1L;

}
