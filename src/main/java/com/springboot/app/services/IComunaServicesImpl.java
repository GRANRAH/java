package com.springboot.app.services;


import java.util.List;
import java.util.Optional;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.springboot.app.dao.IComunaDao;

import com.springboot.app.models.Comuna;




@Service
public class IComunaServicesImpl implements IComunaService {

	@Autowired
	IComunaDao comunaDao;

	
	@Override
	@Transactional(readOnly = true)
	public List<Comuna> getAllComunas() {
		
		return (List<Comuna>) comunaDao.findAll();
	}

	@Override
	@Transactional
	public Comuna guardarComuna(Comuna com) {
		
		return comunaDao.save(com);
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<Comuna> getComunaById(Long id) {
		
		return comunaDao.findById(id);
	}

	@Override
	@Transactional
	public Boolean eliminarComuna(Comuna com) {
		
		try {
			comunaDao.delete(com);
			return true;
			
		}catch (Exception e) {
			System.out.println("error :" + e);
			
		}
		return false;
	}


	
	
	

	
	

	


	
}
