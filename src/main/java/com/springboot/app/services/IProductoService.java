package com.springboot.app.services;


import java.util.List;
import java.util.Optional;

import com.springboot.app.models.Producto;


public interface IProductoService {

	List<Producto> getAllProductos();
	Producto guardarProducto(Producto prod);
	Optional<Producto> getProductoById(Long id);
	Boolean eliminarProducto(Producto prod);
	
}
